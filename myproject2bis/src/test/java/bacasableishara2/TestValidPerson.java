package bacasableishara2;

import org.junit.Test;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import static junit.framework.TestCase.assertEquals;

public class TestValidPerson {
    public static  ValidatorFactory factory;
    public static Validator validator;

    /*
    @BeforeAll
    public static void setUpClass(){
        factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }
     */

    @Test
    public void validateAdult(){
        factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        Calendar calendar = Calendar.getInstance();
        calendar.set(1997,Calendar.DECEMBER,23);
        Date birthDate = calendar.getTime();

        Person p1 = new Person("Ishara","CHAN-TUNG",birthDate,"french",21);
        Set<ConstraintViolation<Person>> constraintViolations = this.validator.validate(p1);
        assertEquals(0,constraintViolations.size());
    }

    @Test
    public void validateChild(){
        factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017,Calendar.DECEMBER,23);
        Date birthDate = calendar.getTime();

        Person p2 = new Person("Ishara","CHAN-TUNG",birthDate,"french",1);
        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p2);
        assertEquals(1,constraintViolations.size());
    }
}
