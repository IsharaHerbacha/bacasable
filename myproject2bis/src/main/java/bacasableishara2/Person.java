package bacasableishara2;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Person {
    @NotNull
    @Size(min=1)
    private String firstName;
    @NotNull
    @Size(min=1)
    private String lastName;
    @NotNull //(groups={AdultCheck.class})
    private Date birthDate;
    private String cityzenship;
    @NotNull //(groups={AdultCheck.class})
    @Min(value=18) //,groups={AdultCheck.class})
    private transient Integer age;

    public Person(String fn,String ln,Date birthDate, String cs, Integer age){
        this.firstName = fn;
        this.lastName = ln;
        this.birthDate = birthDate;
        this.cityzenship = cs;
        this.age = age;
    }
}
